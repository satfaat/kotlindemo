import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Dem1Test {
    @Test
    fun testJavaInteroperability() {
        val javaObj = JavaClass()
        assertEquals("Hello from Java", javaObj.message)
    }
}