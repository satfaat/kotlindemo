package syntax.functions

import syntax.functions.extension.singleQuoted

class ScopeFun {
}


private val title1 = "The Robots from Planet X3"

//RUN
private val newTitle1 = title1
    .removePrefix("The ")
    .run { "'$this'" }
    .uppercase()

// LET
private val newTitle2 = title1
    .removePrefix("The ")
    .let { titleWithoutPrefix -> "'$titleWithoutPrefix'" }
    //.let { "'$it'" }
    .uppercase()

// Also
private val newTitle3 = title1
    .removePrefix("The ")
    .also { println(it) } // Robots from Planet X3
    .singleQuoted()
    .uppercase()


fun main() {

    println(newTitle1)
    println(newTitle2)
    println(newTitle3)
}
