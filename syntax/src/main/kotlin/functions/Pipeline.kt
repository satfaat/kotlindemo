package syntax.functions

class Pipeline {
}

private fun Int.addOne(): Int = this + 1
private fun Int.double(): Int = this * 2


fun main() {
    val value = 5
    val result = value.addOne().double()
    println(result) // Output: 12
}