package syntax.functions


private fun dem(user: Pair<String, String>) {
    println(user.first)
    println(user.second)
}

private val user = "auto21" to "321"

fun main() {

    dem(user)
}