package syntax.functions

class PipelineWithLet {
}

private fun addOne(x: Int): Int = x + 1
private fun double(x: Int): Int = x * 2

fun main() {
    val value = 5
    val result = value.let { addOne(it) }.let { double(it) }
    val result2 = double(addOne(value))
    println(result) // Output: 12
    println(result2)
}