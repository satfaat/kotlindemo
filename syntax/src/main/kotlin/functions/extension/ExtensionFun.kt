package syntax.functions.extension

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ExtensionFun {
}

private val today = LocalDate.now()

fun LocalDate.format2(formatPattern: String): String {
    val formatter = DateTimeFormatter.ofPattern(formatPattern)
    return this.format(formatter)
}

fun LocalDate.format3(formatPattern: String): String = this.format(DateTimeFormatter.ofPattern(formatPattern))

fun String?.singleQuoted() =
    if (this == null) "(no value)" else "'$this'"

val title: String? = null
val newTitle = title.singleQuoted()


val String.isLong: Boolean
    get() = this.length > 20
val string = "This string is long enough"
val isItLong = string.isLong


fun main() {

    println(today.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))
    println(today.toString())
    println(today.format2("dd.MM.yyyy"))
    println(today.format3("dd.MM.yyyy"))

    println(newTitle) // (no value)
}