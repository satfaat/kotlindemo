package syntax.functions

class AnonymousFn {
}

val stringLengthFunc: (String) -> Int = { input ->
    input.length
}

// Higher-order functions
fun stringMapper(str: String, mapper: (String) -> Int): Int {
    // Invoke function
    return mapper(str)
}

val applyDiscount: (Double) -> Double = { price: Double -> price - 5.0 }
val applyDiscount2: (Double) -> Double = { price -> price - 5.0 }
val applyDiscount3: (Double) -> Double = { it - 5.0 }

val withFiveDollarsOff2 = calculateTotal(
    20.0,
    { price ->
        val result = price - 5.0
        println("Initial price: $price")
        println("Discounted price: $result")
        result
    })

fun main() {
    println(stringLengthFunc("Android"))

    val stringLength: Int = stringMapper("Android") { input ->
        input.length
    }

    println(stringLength)
}