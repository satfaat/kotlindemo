package syntax.functions


infix fun <T, R> T.pipe(transform: (T) -> R): R = transform(this)

private fun addOne(x: Int): Int = x + 1
private fun double(x: Int): Int = x * 2

fun main() {
    val value = 5
    val result = value pipe ::addOne pipe ::double
    println(result) // Output: 12
}