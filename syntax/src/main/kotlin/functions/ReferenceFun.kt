package syntax.functions

class ReferenceFun {
}

fun calculateTotal(
    initialPrice: Double,
    applyDiscount: (Double) -> Double,
    taxMultiplier: Double = 1.9
): Double {
    // Apply coupon discount
    val priceAfterDiscount = applyDiscount(initialPrice)
    // Apply tax
    val total = priceAfterDiscount * taxMultiplier
    return total
}

fun discountFiveDollars(price: Double): Double = price - 5.0
fun discountTenPercent(price: Double): Double = price * 0.9
fun noDiscount(price: Double): Double = price

fun discount(couponCode: String): (Double) -> Double = when (couponCode) {
    "FIVE_BUCKS" -> ::discountFiveDollars
    "TAKE_10" -> ::discountTenPercent
    else -> ::noDiscount
}

fun main() {
    val withFiveDollarsOff0 = calculateTotal(20.0, ::discountFiveDollars) // $16.35
    val withTenPercentOff0 = calculateTotal(20.0, ::discountTenPercent) // $19.62
    val fullPrice0 = calculateTotal(20.0, ::noDiscount)

    val withFiveDollarsOff = calculateTotal(20.0, discount("FIVE_BUCKS")) // $16.35
    val withTenPercentOff = calculateTotal(20.0, discount("TAKE_10")) // $19.62
    val fullPrice = calculateTotal(20.0, discount("NONE")) // $21.80
}