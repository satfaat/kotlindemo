package syntax.functions

import syntax.type.price

val withFiveDollarsOff = calculateTotal(20.0, { price -> price - 5.0 }) // $16.35
val withTenPercentOff = calculateTotal(20.0, { price -> price * 0.9 })  // $19.62
val fullPrice = calculateTotal(20.0, { price -> price })  // $21.80


fun printSubtotal(applyDiscount: (Double) -> Double): String {
    val result = applyDiscount(20.0)
    val formatted = "$%.2f".format(result)
    println(formatted)
    return formatted
}

fun discount2(couponCode: String): (Double) -> Double = when (couponCode) {
    "FIVE_BUCKS" -> { price -> price - 5.0 }
    "TAKE_10" -> { price -> price * 0.9 }
    else -> { price -> price }
}

fun discount3(couponCode: String): (Double) -> Double = when (couponCode) {
    "FIVE_BUCKS" -> dollarAmountDiscount(5.0)
    "NINE_BUCKS" -> dollarAmountDiscount(9.0)
    "TAKE_10" -> percentageDiscount(0.10)
    "TAKE_15" -> percentageDiscount(0.15)
    else -> { price -> price }
}

fun dollarAmountDiscount(dollarsOff: Double): (Double) -> Double =
    { price -> price - dollarsOff }

fun percentageDiscount(percentageOff: Double): (Double) -> Double {
    val multiplier = 1.0 - percentageOff
    return { price -> price * multiplier }
}

fun main() {
    printSubtotal { price -> price - 5.0 }
    printSubtotal { price -> price * 0.9 }
}