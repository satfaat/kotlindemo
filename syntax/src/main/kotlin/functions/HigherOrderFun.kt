package syntax.functions

// Higher order function


fun performCalculation(x: Int, operation: (Int, Int) -> Int) {
    val result = operation(x, 5)
    println(result)
}

val add = { a: Int, b: Int -> a + b }

fun greet(message: String, printFunction: (String) -> Unit) {
    val greeting = "Hello, $message"
    printFunction(greeting)
}

fun printMessage(message: String) {
    println(message)
}


fun main() {
    performCalculation(10, add)  // Pass the lambda expression as an argument
    greet("World", ::printMessage)
}