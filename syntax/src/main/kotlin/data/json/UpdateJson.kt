package syntax.data.json

import org.json.JSONObject
import java.io.File

object JsonContent {

    fun d1Doc(): String {
        val filePath = ".share/dbs/d1.json"
        // Read JSON from file
        return File(filePath).readText()
    }

    fun dealsDoc(): String {
        val filePath = ".share/dbs/deals.json"
        return File(filePath).readText()
    }
}

class UpdateJson() {
    fun updateJsonObject() {
        val filePath = ".share/dbs/d1.json"
//
//        // Read JSON from file
//        val jsonString = File(filePath).readText()

        // Parse JSON into JSONObject
        val jsonObject = JSONObject(JsonContent.d1Doc())

        // Modify data
        val newAge = jsonObject.getInt("age") + 1
        jsonObject.put("age", newAge)

        // Write updated JSON back to file
        File(filePath).writeText(jsonObject.toString())
    }

    fun write(json: JSONObject, fileName: String) {
        val filePath = ".share/dbs/$fileName.json"
        File(filePath).writeText(json.toString())
    }
}

val nums = listOf("0000041290", "0000041370")

fun main() {
    val jsonObject = JSONObject(JsonContent.dealsDoc())
    println(jsonObject)

    // Modify data
    val newDealNum = "1051900"

    //Deals > 1
    jsonObject.getJSONObject("Deals")
        .getJSONObject("1")
        .put("deal_number", newDealNum)

    jsonObject.getJSONObject("Deals")
        .getJSONObject("1")
        .put("payment_numbers", nums)

    // Write updated JSON back to file
    UpdateJson().write(jsonObject, "deals")
}