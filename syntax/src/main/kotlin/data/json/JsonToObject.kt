package syntax.data.json

import com.google.gson.Gson
import kotlinx.serialization.json.Json
import java.io.File
import java.lang.reflect.Type

class JsonToObject {
    private val gson = Gson()

    // Method to read JSON file and return object of specified type
    fun <T> getObjFromGson(filePath: String, type: Type): T? {
        val jsonString = File(filePath).readText()
        return gson.fromJson(jsonString, type)
    }

    inline fun <reified T> getDataFromJSON(jsonString: String, tbName: String, id: String): T? {
        try {
            val allData = Json.decodeFromString<Map<String, Map<String, T>>>(jsonString)
            val data: T? = allData[tbName]?.get(id)
            return data
        } catch (e: Exception) {
            println("Error decoding JSON: $e")
            // You could throw a specific exception or return a custom error object here
            return null
        }
    }
}