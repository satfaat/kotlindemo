package syntax.data.json

import org.json.JSONObject


fun main() {

    val dealsDocument = JSONObject(JsonContent.dealsDoc())
    println(dealsDocument)

    val dealNum = dealsDocument
        .getJSONObject("Deals")
        .getJSONObject("1")
        .getString("deal_number")
    println(dealNum)

    val paymentNums = mutableListOf<String>()
    dealsDocument
        .getJSONObject("Deals")
        .getJSONObject("1")
        .getJSONArray("payment_numbers").forEach() {
            paymentNums.add(it.toString())
        }
    val paymentNums1 = paymentNums.toList()
    println(paymentNums)
    println(paymentNums.toList())
    println(paymentNums.javaClass)
    println(paymentNums.javaClass.kotlin)
    println(paymentNums::class)
    println(paymentNums1)

//    val newList = mutableListOf<String>()
//    paymentNums.forEach() {
//        newList.add(it.toString())
//    }
//    println(newList)

}