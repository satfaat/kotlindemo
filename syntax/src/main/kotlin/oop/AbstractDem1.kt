package syntax.oop


class Airplane(private val name: String) : Aircraft() {
    override fun getName(): String = name
}

abstract class Aircraft {
    init {
        println("Aircraft = ${getName()}")
    }

    abstract fun getName(): String

    fun printName() {
        println("Aircraft = ${getName()}")
    }
}


fun main() {
    val airplane = Airplane("Boeing 747")
    airplane.getName() //Aircraft = null
    airplane.printName()
}