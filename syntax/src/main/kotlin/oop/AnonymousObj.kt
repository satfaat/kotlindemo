package syntax.oop

class AnonymousObj {
}

data class ProductParams(
    val code: String,
    val sum: String,
    val commission: String
)

private val productParams = mutableListOf(
    object {
        val code = "0000"
        val sum = 25_000_000_000.toString()
        val commission = 7.0.toString()
    },
    object {
        val code = "1440"
        val sum = 50_000_000.toString()
        val commission = 25.0.toString()
    }
)

private val productParams2 = mutableListOf(
    ProductParams("0000", 25_000_000_000.toString(), 7.0.toString()),
    ProductParams("1440", 50_000_000.toString(), 25.0.toString())
)

fun main() {

    productParams2.forEach() {
        println(it.code)
    }
//    println(productParams[0].code)
}