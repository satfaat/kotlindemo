package syntax.oop


fun eval(expr: Expr): Int =
    when (expr) {
        is Num -> expr.value
        is Sum -> eval(expr.left) + eval(expr.right)
        else -> throw IllegalArgumentException("Unknown expression")
    }

interface Expr
class Num(val value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr

fun main() {
    val num1: Expr = Num(5)
    val num2: Expr = Num(3)
    val sum: Expr = Sum(num1, num2)
    println(eval(num1))
    println(eval(sum))
}