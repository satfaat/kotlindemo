package syntax.generics


sealed interface Beverage {
    val idealTemperature: Int
}

enum class Coffee : Beverage {
    LIGHT_ROAST, MEDIUM_ROAST, DARK_ROAST;

    override val idealTemperature: Int = 135
}

enum class Tea : Beverage {
    GREEN_TEA, BLACK_TEA, RED_TEA;

    override val idealTemperature: Int = 140
}

//class CoffeeMug(val coffee: Coffee)
//class TeaMug(val tea: Tea)
class Mug<T : Beverage>(val beverage: T) {
    val temperature = beverage.idealTemperature
}

fun drink(coffee: Coffee) = println("Drinking coffee: $coffee")
fun drink(tea: Tea) = println("Drinking tea: $tea")


fun main() {
    val mug = Mug<Coffee>(Coffee.LIGHT_ROAST)
    val mugOfTea = Mug(Tea.BLACK_TEA)

    drink(mug.beverage)
    drink(mugOfTea.beverage)
}