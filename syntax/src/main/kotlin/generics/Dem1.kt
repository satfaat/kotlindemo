package syntax.generics




fun <T : Beverage> serve(beverage: T): Mug<T> = Mug(beverage)

val mug = serve<Coffee>(Coffee.DARK_ROAST)
val mug1 = serve(Tea.GREEN_TEA)

fun <T : Beverage> T.pourIntoMug() = Mug(this)
val mug2 = Coffee.DARK_ROAST.pourIntoMug()

fun main() {}