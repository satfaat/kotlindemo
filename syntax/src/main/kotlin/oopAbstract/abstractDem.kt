package syntax.oopAbstract

import syntax.oopInterface.model.Named


open class Clunker(acceleration: Double) : Car3(acceleration) {
    override fun makeEngineSound() {
        println("putt-putt-putt")
    }
}

class Junker : Clunker(0.0)

class SimpleCar(acceleration: Double) : Car2(acceleration)
class NamedCar(override val name: String) : Car3(3.0), Named

val clunker = Clunker(0.25)
val car = SimpleCar(1.2)

fun main() {
    clunker.accelerate()
    car.accelerate()
    car
}