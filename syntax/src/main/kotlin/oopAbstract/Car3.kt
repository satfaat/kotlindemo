package syntax.oopAbstract

open class Car3(private val acceleration: Double = 1.0) {
    protected var speed = 0.0
        private set

    protected open fun makeEngineSound() = println("Vrrrrrr...")

    fun accelerate() {
        speed += acceleration
        makeEngineSound()
    }
}