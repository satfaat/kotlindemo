package syntax.oopAbstract

class MuscleCar : Car3(5.0) {
    override fun makeEngineSound() = when {
        speed < 10.0 -> println("Vrooooom")
        speed < 20.0 -> println("Vrooooooooom")
        else -> println("Vrooooooooooooooooooom!")
    }
}