package syntax.oopAbstract

abstract class Car(private val acceleration: Double = 1.0) {
    private var speed = 0.0
    protected abstract fun makeEngineSound()

    fun accelerate() {
        speed += acceleration
        makeEngineSound()
    }
}