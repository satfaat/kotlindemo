package syntax


class HolidayException(val task: String) : Exception("'$task' is not allowed on holidays")

private val ordinals = listOf("zeroth", "first", "second", "third", "fourth", "fifth")
private fun ordinal(number: Int) = ordinals.get(number)

private fun annc(number: Int, task: String): String {
    if ("clean" in task) throw HolidayException(task)
    val ordinal = ordinal(number)
    return "The $ordinal thing I will do is $task."
}

private val tasks = listOf(1 to "clean my room", 9 to "take out trash", 5 to "feed the dog")


fun main() {
//    val first = annc(1, "clean my room")
//    println(first)


    tasks.forEach { (number, task) ->
        try {
            println(annc(number, task))
        } catch (e: HolidayException) {
            println("It's a holiday! I'm not going to ${e.task} today!")
//            e.stackTrace.reversed().let { println(it) }
//            e.stackTrace.reversed().drop(1).let { println(it) }
            e.stackTrace
                .reversed()
                .drop(1)
                .joinToString(" -> ") { "${it.methodName}()" }
                .let { println("Error: $it") }
        } catch (e: ArrayIndexOutOfBoundsException) {
            println("I can't count that high!")
        } catch (e: Exception) {
            println("I wasn't expecting this!")
        }
    }
    println("\n")
    tasks.forEach { (number, task) ->
        val words: String = try {
            annc(number, task)
        } catch (e: HolidayException) {
            "It's a holiday! I'm not going to ${e.task} today!"
        } catch (e: ArrayIndexOutOfBoundsException) {
            "I can't count that high!"
        }
        println(words)
    }

    println("\n")
    tasks.forEach { (number, task) ->
        val result = runCatching { annc(number, task) }
        val text = result.getOrDefault("Something went wrong!")
        val text2 = result.getOrElse { "Something went wrong! ${it.message}" }
        println(text2)
    }
}