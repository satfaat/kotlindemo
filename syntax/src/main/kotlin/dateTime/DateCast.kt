package syntax.dateTime

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

val strDate = "25.08.2027"
val format = SimpleDateFormat("dd.MM.yyyy") // Specify the date format
val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")


fun dateCast(strDate: String) {
    try {
        val date = format.parse(strDate)
        println(date) // Prints the date object
    } catch (e: Exception) {
        println("Invalid date format: $e")
    }
}

fun StrToDate(str: String, formatter: DateTimeFormatter) {
    val date = formatter.parse(str)
}

fun main() {

    println(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))

    dateCast(strDate)
    println(formatter.parse(strDate))

    val date1 = LocalDate.parse("2022-01-06")
    val date2 = LocalDate.parse("25.08.2027", DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    println("Date2: $date2")

    // formated to string
    //.toJavaLocalDate().format(formatter)
}

