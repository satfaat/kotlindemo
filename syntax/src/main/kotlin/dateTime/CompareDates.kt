package syntax.dateTime

import java.time.LocalDate

class CompareDates {
}


fun main(){
    val today = LocalDate.now()
    val birthday = LocalDate.parse("1990-01-01")
    println(today)


    val isBirthday = today == birthday  // false (today is not the birthday)
    val isAfterBirthday = today > birthday // true (today is after the birthday)

    println(isBirthday) // Prints false
    println(isAfterBirthday)

    println(today.plusDays(11))
    println(today.plusDays(-11))
}