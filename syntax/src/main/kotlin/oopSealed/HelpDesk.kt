package syntax.oopSealed

object HelpDesk {
    fun handle(request: SupportRequest2) =
        println("Help desk is handling ${request.id}")
}