package syntax.oopSealed

sealed interface Request {
    val id: Int
}