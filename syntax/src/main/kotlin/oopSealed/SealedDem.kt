package syntax.oopSealed

val order = OrderRequest(123, Size.CUP)
val refund = RefundRequest(456, Size.CUP, "bad quality")
val request = SupportRequest(789, "I can't open the bag of ice!")

val order2 = OrderRequest2(Size.CUP)
val refund2 = RefundRequest2(Size.CUP, "bad quality")
val request2 = SupportRequest2("I can't open the bag of ice!")

fun main() {
    FrontDesk.receive(order2)
    FrontDesk.receive(refund2)
    FrontDesk.receive(request2)
}

