package syntax.oopSealed

object FrontDesk {
    fun receive(request: Request2) {
        println("Handling request #${request.id}")
        when (request) {
            is OrderRequest2 -> IceCubeFactory.fulfillOrder(request)
            is RefundRequest2 -> IceCubeFactory.fulfillRefund(request)
            is SupportRequest2 -> HelpDesk.handle(request)
            else -> {"In develope"}
        }
    }
}