package syntax.oopSealed

object IceCubeFactory {
    fun fulfillOrder(order: OrderRequest2) = println("Fulfilling order #${order.id}")
    fun fulfillRefund(refund: RefundRequest2) = println("Fulfilling refund #${refund.id}")
}