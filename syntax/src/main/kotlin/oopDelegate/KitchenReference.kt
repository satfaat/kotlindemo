package syntax.oopDelegate

enum class Beverage { WATER, SODA, PEACH_ICED_TEA, TEA_LEMONADE }
enum class Entree { TOSSED_SALAD, SALMON_ON_RICE }
enum class Appetizer { SALAD }
enum class Dessert { CAKE }