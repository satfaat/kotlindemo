package syntax.oopDelegate


class Chef2 : KitchenStaff {
    override val specials: List<String>
        get() = listOf("Super")

    override fun prepareEntree(name: String): Entree? = when (name) {
        "Tossed Salad" -> Entree.TOSSED_SALAD
        "Salmon on Rice" -> Entree.SALMON_ON_RICE
        else -> null
    }

    override fun prepareAppetizer(name: String): Appetizer? {
        TODO("Not yet implemented")
    }

    override fun prepareDessert(name: String): Dessert? {
        TODO("Not yet implemented")
    }

    override fun receiveCompliment(message: String) =
        println("Chef received a compliment: $message")
}

class Waiter2(private val chef: Chef2) : KitchenStaff {
    // The waiter can prepare beverages by himself...
    fun prepareBeverage(name: String): Beverage? = when (name) {
        "Water" -> Beverage.WATER
        "Soda" -> Beverage.SODA
        else -> null
    }

    // ... but needs the chef to prepare the entrees
    // Manually delegating to the chef for all of these things:
    override val specials: List<String> get() = chef.specials
    override fun prepareEntree(name: String) = chef.prepareEntree(name)
    override fun prepareAppetizer(name: String) = chef.prepareAppetizer(name)
    override fun prepareDessert(name: String) = chef.prepareDessert(name)
    override fun receiveCompliment(message: String) = chef.receiveCompliment(message)

    fun acceptPayment(money: Int) = println("Thank you for paying for your meal")
}

class Waiter3(
    private val chef: Chef2,
    private val bartender: Bartender
) : KitchenStaff by chef, BarStaff by bartender {
    fun acceptPayment(money: Int) = println("Thank you for paying for your meal")

    override fun prepareEntree(name: String): Entree? =
        if (name == "Tossed Salad") Entree.TOSSED_SALAD else chef.prepareEntree(name)

    override fun receiveCompliment(message: String) = when {
        message.contains("entree") -> chef.receiveCompliment(message)
        message.contains("beverage") -> bartender.receiveCompliment(message)
        else -> println("Waiter received compliment: $message")
    }
}