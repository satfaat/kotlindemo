package syntax.oopDelegate

class Chef {
    fun prepareEntree(name: String): Entree? = when (name) {
        "Tossed Salad" -> Entree.TOSSED_SALAD
        "Salmon on Rice" -> Entree.SALMON_ON_RICE
        else -> null
    }
}

class Waiter(private val chef: Chef) {
    // The waiter can prepare a beverage by himself...
    //This is manual delegation to Chef class
    fun prepareBeverage(name: String): Beverage? = when (name) {
        "Water" -> Beverage.WATER
        "Soda" -> Beverage.SODA
        else -> null
    }

    // ... but needs the chef to prepare an entree
    fun prepareEntree(name: String): Entree? = chef.prepareEntree(name)

    fun acceptPayment(money: Int) = println("Thank you for paying for your meal")
}

// Customer act
val waiter = Waiter3(Chef2(), Bartender())

val beverage = waiter.prepareBeverage("Soda")
val entree = waiter.prepareEntree("Salmon on Rice")

class Cow : Eater by Muncher("grass")
class Chicken : Eater by Muncher("bugs")
class Pig : Eater by Muncher("corn")
class Pig2 : Eater {
    override fun eat() = println("Scarfing down corn - NOM NOM NOM!!!")
}
class Pig3 : Eater by Scarfer("corn")


fun main() {
    waiter.receiveCompliment("The salmon entree was fantastic!")
    waiter.receiveCompliment("The peach tea beverage was fantastic!")
    waiter.receiveCompliment("The service was fantastic!")

    Cow().eat()     // Eating grass - munch, munch, munch!
    Chicken().eat() // Eating bugs - munch, munch, munch!
    Pig().eat()     // Eating corn - munch, munch, munch!
    Pig2().eat()
    Pig3().eat()
}