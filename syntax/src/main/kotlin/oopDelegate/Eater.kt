package syntax.oopDelegate

interface Eater {
    fun eat()
}