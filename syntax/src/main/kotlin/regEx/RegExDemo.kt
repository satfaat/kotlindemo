package syntax.regEx

import java.util.regex.Pattern

class RegExDemo {
}

private val month = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"
fun getPattern(): String = """\d{2} $month \d{4}"""

fun main() {
    println(Pattern.compile("Welcome, .*"))

    val regex = Pattern.compile("\\d+")
    val matcher = regex.matcher("Hello 123 World 456")

    while (matcher.find()) {
        println("Match found: ${matcher.group()}")
    }

    println(getPattern())
}