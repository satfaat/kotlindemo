package syntax.collections.map

class AssociatingData {
}

// Pair
private val association = Pair("Nail", "Hammer")
private val association2 = "Nail".to("Hammer")
private val association3: Pair<String, String> = "Nail" to "Hammer" // infix function


fun main() {

    println(association)
    println(association.first)
    println(association.javaClass) // type Pair
}