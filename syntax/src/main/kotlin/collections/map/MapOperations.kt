package syntax.collections.map


private var toolbox = mapOf(
    "Nail" to "Hammer",
    "Hex Nut" to "Wrench",
    "Hex Bolt" to "Wrench",
    "Slotted Screw" to "Slotted Screwdriver",
    "Phillips Screw" to "Phillips Screwdriver",
)

fun main() {

    // type: Map.Entry<String, String>
    toolbox.forEach { entry ->
        println("Use a ${entry.value} on a ${entry.key}")
    }

    val screwdrivers = toolbox.filter { entry ->
        entry.value.contains("Screwdriver")
    }
    println(screwdrivers)

    val screwdriversByKeys = toolbox.filter { entry ->
        entry.key.contains("Screw")
    }
    println(screwdriversByKeys)

    val newToolbox = toolbox
        .mapKeys { entry -> entry.key.replace("Hex", "Flange") }
        .mapValues { entry -> entry.value.replace("Wrench", "Ratchet") }
    println(newToolbox)
}