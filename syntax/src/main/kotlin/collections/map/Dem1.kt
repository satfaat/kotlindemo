package syntax.collections.map

private const val POINTS_X_PASS: Int = 15
private val EZPassAccounts: MutableMap<Int, Int> = mutableMapOf(1 to 100, 2 to 100, 3 to 100)
private val EZPassReport: Map<Int, Int> = EZPassAccounts

private fun updatePointsCredit(accountId: Int) {
    if (EZPassAccounts.containsKey(accountId)) {
        println("Updating $accountId...")
        EZPassAccounts[accountId] = EZPassAccounts.getValue(accountId) + POINTS_X_PASS
    } else {
        println("Error: Trying to update a non-existing account (id: $accountId)")
    }
}

private fun accountsReport() {
    println("EZ-Pass report:")
    EZPassReport.forEach { (k, v) ->
        println("ID $k: credit $v")
    }
}

fun main() {
    accountsReport()
    updatePointsCredit(1)
    updatePointsCredit(1)
    updatePointsCredit(5)
    accountsReport()

    println(EZPassAccounts.get(1))
    println(EZPassAccounts.getValue(2))

    val numbers = listOf(1, -2, 3, -4, 5, -6)

    val doubled = numbers.map { x -> x * 2 }
    println(doubled)
    val tripled = numbers.map { it * 3 }
    println(tripled)
}