package syntax.collections.map

class Tool(
    val name: String,
    val weightInOunces: Int,
    val correspondingHardware: String,
)

val tools = listOf(
    Tool("Hammer", 14, "Nail"),
    Tool("Wrench", 8, "Hex Nut"),
    Tool("Wrench", 8, "Hex Bolt"),
    Tool("Slotted Screwdriver", 5, "Slotted Screw"),
    Tool("Phillips Screwdriver", 5, "Phillips Screw"),
)

private val toolbox = tools.associate { tool ->
    tool.correspondingHardware to tool.name
}

val toolsByName = tools.associateBy { tool -> tool.name }

val toolWeightInPounds = tools.associateWith { tool ->
    tool.weightInOunces * 0.0625
}

private val toolbox2 = tools.associateBy({ it.correspondingHardware }, { it.name })

val toolsByWeight = tools.groupBy { tool ->
    tool.weightInOunces
}

val toolNamesByWeight = tools.groupBy(
    { tool -> tool.weightInOunces },
    { tool -> tool.name }
)

fun main() {
    println(toolbox)

    println(toolsByName)

    val hammer = toolsByName["Hammer"]
    println(hammer?.correspondingHardware)

    println(toolWeightInPounds)
    val hammerWeightInPounds = toolWeightInPounds[hammer]
    println(hammerWeightInPounds)

    println(toolbox2)

    println(toolsByWeight)
    println(toolsByWeight[14]) // List<Tool>
    println(toolsByWeight[14]?.javaClass)
    println(toolsByWeight[14]?.get(0)) // Tool
    println(toolsByWeight[14]?.get(0)?.javaClass)
    println(toolsByWeight[14]?.get(0)?.name) // String
    println()

    println(toolNamesByWeight)
}