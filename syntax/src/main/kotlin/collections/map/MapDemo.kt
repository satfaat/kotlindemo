package syntax.collections.map

private val toolbox = mapOf(
    "Nail" to "Hammer",
    "Hex Nut" to "Wrench",
    "Hex Bolt" to "Wrench",
    "Slotted Screw" to "Slotted Screwdriver",
    "Phillips Screw" to "Phillips Screwdriver",
)

fun main() {
    println(toolbox)

    val tool = toolbox.get("Nail")
    val tool2 = toolbox["Nail"]
    val tool3 = toolbox.getValue("Nail")
    println(tool) // Hammer

    val tool4 = toolbox.getOrDefault("Hanger Bolt", "Hand")
    println(tool4)

    // Add new entry
    val toolbox = toolbox.toMutableMap()
    toolbox.put("Lumber", "Saw")
    toolbox["Lumber"] = "Saw"
    // change value
    toolbox["Hex Bolt"] = "Nut Driver"
    // remove entry
    toolbox.remove("Lumber")
    println(toolbox)

    // Get default
    val tool1 = toolbox.getOrDefault("Hanger Bolt", "Hand")
    val anotherTool = toolbox.getOrDefault("Dowel Screw", "Hand")
    val oneMoreTool = toolbox.getOrDefault("Eye Bolt", "Hand")
    println(tool1)
    println(anotherTool)
    println(oneMoreTool)
}