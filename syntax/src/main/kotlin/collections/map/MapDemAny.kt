package syntax.collections.map

private val map1: Map<String, Any> = mapOf(
    "k1" to "bird",
    "k2" to 20,
)

private val mainMap: Map<String, Any> = mapOf(
    "nestedMap" to map1  // Key "nestedMap" holds the first map (map1)
)

private val mainMap2: Map<String, Map<String, Any>> = mapOf(  // Specify key and value types
    "nestedMap" to map1
)

fun main() {
    println(mainMap)

    val nestedMap = mainMap["nestedMap"] as Map<*, *>  // Cast to specific type
    val birdValue = nestedMap["k1"]

    println(birdValue)  // Output: bird
}