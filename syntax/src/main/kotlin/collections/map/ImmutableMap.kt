package syntax.collections.map

private var toolbox = mapOf(
    "Nail" to "Hammer",
    "Hex Nut" to "Wrench",
    "Hex Bolt" to "Wrench",
    "Slotted Screw" to "Slotted Screwdriver",
    "Phillips Screw" to "Phillips Screwdriver",
)

fun main() {

    // Add an entry
    toolbox = toolbox + Pair("Lumber", "Saw")

// Update an entry
    toolbox = toolbox + Pair("Hex Bolt", "Nut Driver")

// Remove an entry
    toolbox = toolbox - "Lumber"

// Simulate changing a key
    toolbox = toolbox - "Phillips Screw"
    toolbox = toolbox + Pair("Cross Recess Screw", "Phillips Screwdriver")

    println(toolbox)

    // default value
    toolbox = toolbox.withDefault { key -> "Hand" }
    println(toolbox)

    val tool = toolbox.getValue("Hanger Bolt")
    val anotherTool = toolbox.getValue("Dowel Screw")
    val oneMoreTool = toolbox.getValue("Eye Bolt")
    println(tool)
    println(anotherTool)
    println(oneMoreTool)
}

