package syntax.collections


val wordArray = arrayOf(
    arrayOf("24/7", "multi-tier", "B-to-B", "dynamic", "pervasive"),
    arrayOf("empowered", "leveraged", "aligned", "targeted"),
    arrayOf("process", "paradigm", "solution", "portal", "vision")
)

val wordArraySize = arrayOf(
    wordArray[0].size,
    wordArray[1].size,
    wordArray[2].size
)

val rands = arrayOf(
    (Math.random() * wordArraySize[0]).toInt(),
    (Math.random() * wordArraySize[1]).toInt(),
    (Math.random() * wordArraySize[2]).toInt()
)

// String template
val phrase = "${wordArray[0][rands[0]]} ${wordArray[1][rands[1]]} ${wordArray[2][rands[2]]}"

var result = "wordArray is ${if (wordArray.size > 10) "large" else "small"}"

fun main() {
    println(wordArray[0].size)
    wordArraySize.forEach { print(it) }
    println()
    rands.forEach { print(it) }
    println("\n$phrase")

    println(result)
}