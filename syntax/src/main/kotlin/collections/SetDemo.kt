package syntax.collections


// Set for ensuring that each element in it is
//always unique.

// does not guarantee the order of its elements when you print them out

private val booksBySlim: MutableSet<String> = mutableSetOf(
    "The Malt Shop Caper",
    "Who is Mrs. W?",
    "At Midnight or Later",
)

private val booksBySlim0: Set<String> = mutableSetOf(
    "The Malt Shop Caper",
    "Who is Mrs. W?",
    "At Midnight or Later",
)

private val bookList = listOf(
    "The Malt Shop Caper",
    "At Midnight or Later",
    "The Malt Shop Caper",
)

val bookSet = bookList.toSet() // bookSet has two elements
val anotherBookList = bookSet.toList() // anotherBookList also has two elements

fun main() {

    booksBySlim.add("The Malt Shop Caper")
    println(booksBySlim)

    booksBySlim0.toMutableSet().add("The Malt Shop Caper")
    println(booksBySlim0)
}