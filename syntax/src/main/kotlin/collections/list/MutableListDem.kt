package syntax.collections.list

class MutableListDem {
}

private val actualValues: MutableList<Any> = mutableListOf<Any>()


fun main() {
    println(actualValues)
    actualValues.add("New Value")
    actualValues.add(10)
    println(actualValues) // Prints "[New Value, 10]"
}