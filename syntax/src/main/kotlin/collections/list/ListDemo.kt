package syntax.collections.list

// [0.25 five times]
val l1 = List(5) { 0.25 }

var booksToRead = listOf(
    "Tea with Agatha",
    "Mystery on First Avenue",
    "The Ravine of Sorrows",
    "Among the Aliens",
    "The Kingsford Manor Mystery",
)

val sortableTitles: MutableList<String> = mutableListOf()

val sortableTitles2 = booksToRead
    .map { title -> title.removePrefix("The ") }
    .sorted()
    .filter { title -> title.contains("Mystery") }


fun main() {
    println(l1)

//    booksToRead = booksToRead + "Beyond the Expanse"
//    booksToRead = booksToRead - "Among the Aliens"
    booksToRead = booksToRead + "Beyond the Expanse" - "Among the Aliens"
    println(booksToRead)

    val firstBook = booksToRead.get(0)
    println(firstBook)

    booksToRead.forEach { element ->
        println(element)
    }

    booksToRead.sorted().forEach(::println)

    booksToRead.forEach { title ->
        sortableTitles.add(title.removePrefix("The "))
    }

    sortableTitles.forEach(::println)
    println(sortableTitles2.sorted())
}