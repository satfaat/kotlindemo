package syntax.collections.list

private val systemUsers: MutableList<Int> = mutableListOf(1, 2, 3)
private val sudoers: List<Int> = systemUsers

private fun addSystemUser(newUser: Int) {
    systemUsers.add(newUser)
}

private fun getSysSudoers(): List<Int> {
    return sudoers
}

fun main() {
    addSystemUser(4)
    println("Tot sudoers: ${getSysSudoers().size}")
    getSysSudoers().forEach { i ->
        println("Some useful info on user $i")
    }
    // getSysSudoers().add(5) <- Error!

    val numbers = listOf(1, -2, 3, -4, 5, -6)

    val positives = numbers.filter { x -> x > 0 }
    println(positives)
    val negatives = numbers.filter { it < 0 }
    println(negatives)
}