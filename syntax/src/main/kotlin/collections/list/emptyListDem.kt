package syntax.collections.list

class emptyListDem {
}

private val actualValues = emptyList<Any>()
private val actualValues1 = listOf<Any>()  // Empty list

private val newList = actualValues + "New Value" + 10  // Creates a new list with multiple values


fun main() {
    println(actualValues)
    println(actualValues1)
    println(newList) // Prints "[New Value, 10]" (original list remains empty)
}