package syntax.collections.list

private val highScores = listOf(4000, 2000, 10200, 12000, 9030)

fun main() {
    highScores.withIndex().forEach(::println)
}