package syntax.type

val intToStr = (2584).toString()

val price = 2000.25010
val price2: Double = 280767.12
val priseAsStr = "2,125.00"
val PREMIUM_SUM: Double = 327_657.76
val formattedPrice1 = "%,.0f".format(price)
val formattedPrice2 = "%,.0f".format(price).replace(",", " ")
val WAGE_FUND: Double = 20_000_000.00

fun main() {
    println((1236).toString())
    println(intToStr::class)

    println("\n")
    println(price.toString().replace(".", " "))
    println(formattedPrice1)
    println(formattedPrice2)

//    try {
//        (priseAsStr.toDouble())
//    } catch (e: Error) {
//        e.message
//    }

    try {
        (priseAsStr.toDouble())
    } catch (e: Exception) {
        println(e.message)
    }
    println(priseAsStr.replace(",", "").toDouble())

    val sumToStr = "%,.2f".format(price2)
    println(sumToStr)

    println(
        String
            .format("%,.2f", PREMIUM_SUM)
            .replace(",", " ")
    )

    println("%.3f".format(WAGE_FUND))
}