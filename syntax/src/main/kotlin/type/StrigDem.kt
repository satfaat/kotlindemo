package syntax.type

class StrigDem {
}

val strInit = "Showing 1 to 71 of 70 records"

val regex = """\d+(?= of)""".toRegex()
val matchResult = regex.find(strInit)

fun main(){
    println(matchResult?.value?.toInt() ?: println("Could not find total count"))
    println(matchResult?.value ?: println("Could not find total count"))

    println("2".repeat(20))
}