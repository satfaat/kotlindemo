package syntax.type


val d1: Double = 100000.0
val f1: Float = 9.41f

fun main() {

    println(100000.0 * 9.41)
    println(f1)
    println(f1.toDouble())
    val res1: Double = d1 * f1
    println(res1)

    val res2: Double = d1 * f1.toDouble()
    println(res2)
}