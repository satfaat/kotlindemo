package syntax.type

class DoubleCastDemo {
}

fun main(){
    println(23_000_000.0.toString())
    println("%.2f".format(23_000_000.0))
}