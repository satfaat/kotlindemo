package syntax.type

private fun showHashCode(obj: Any) {
    println("${obj.hashCode()}")
}

fun main() {
    showHashCode(1)
}