package syntax.type

import syntax.regEx.getPattern

val str1 = "AF"

const val soStage = "https://stage-sales.aic.uz/"
const val DOMAIN = soStage
const val SO_INVOICE = "sales/invoice"

val lnk1 = "$DOMAIN$SO_INVOICE"
val lnk2 = DOMAIN + "sales/invoice"


class Dem() {
    fun log() {
        println("Output Dem: " + this::class.java)
    }
}

const val question = "life, the universe, and everything"
const val answer = 42
private val tripleQuotedString = """
    #question = "$question"
    #answer = $answer""".trimMargin()

fun main() {
    println(str1[0].toString())
    println(str1.substring(1))
    println(str1.drop(1))
    println(str1.dropLast(1))

    println(lnk1)
    println(lnk2)

    Dem().log()

    println(tripleQuotedString)
}