package syntax.type

import java.util.*

class UUIDdemo {

    //
}


fun main() {
    //
    val uuid = UUID.randomUUID()

    val uuid_as_str = UUID.randomUUID().toString().replace("-", "").take(12)

    println(uuid)
    println(uuid_as_str)
}