package syntax.oopInterface.model

interface Speaker {
    fun speak() {
        println("...") // default
    }
}