package syntax.oopInterface.model

interface FarmAnimal {
    val name: String
    fun speak()
}