package syntax.oopInterface.model

interface Named {
    val name: String get() = "Nameless"
}