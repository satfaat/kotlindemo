package syntax.oopInterface.farm

import syntax.oopInterface.model.Named
import syntax.oopInterface.model.Speaker

class Cow2(override val name: String) : Speaker, Named {
    override fun speak() = println("Moo!")
}