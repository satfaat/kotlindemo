package syntax.oopInterface.farm

import syntax.oopInterface.model.FarmAnimal

// interface give Chicken membership to FarmAnimal
class Chicken(override val name: String, var numberOfEggs: Int = 0) : FarmAnimal {
    override fun speak() = println("Cluck!")
}