package syntax.oopInterface.farm

import syntax.oopInterface.model.FarmAnimal2

class Cow3(override val name: String) : FarmAnimal2 {
    override fun speak() = println("Moo!")
}