package syntax.oopInterface.farm

import syntax.oopInterface.model.FarmAnimal

class Farmer2(val name: String) {
    fun greet(animal: FarmAnimal) {
        println("Good morning, ${animal.name}!")
        animal.speak()
    }
}