package syntax.oopInterface.farm

import syntax.oopInterface.model.FarmAnimal

// implements FarmAnimal
class Pig(override val name: String, val excitementLevel: Int) : FarmAnimal {
    override fun speak() {
        repeat(excitementLevel) {
            println("Oink!")
        }
    }
}