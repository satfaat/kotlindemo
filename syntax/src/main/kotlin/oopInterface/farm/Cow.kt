package syntax.oopInterface.farm

import syntax.oopInterface.model.FarmAnimal

class Cow(override val name: String) : FarmAnimal {
    override fun speak() = println("Moo!")
}