package syntax.oopInterface

import syntax.oopInterface.farm.*
import syntax.oopInterface.model.FarmAnimal


val sue = Farmer("Sue")
val henrietta = Chicken("Henrietta")
val hamlet = Pig("Hamlet", 1)
val dairyGodmother = Cow("Dairy Godmother")
val snail = Snail("Slick")
val unknown = UnknownAnimal()

val alice = Farmer2("Alice")

val animals: List<FarmAnimal> = listOf(
    Chicken("Henrietta"),
    Pig("Hamlet", 1),
    Cow("Dairy Godmother"),
)


fun main() {
    sue.greet(henrietta)
    sue.greet(hamlet)
    sue.greet(dairyGodmother)

    alice.apply {
        greet(henrietta)
        greet(hamlet)
        greet(dairyGodmother)
    }

    println()
    animals.forEach { alice.greet(it) }
    snail.speak()
    unknown.apply {
        println(name)
        speak()
    }
}