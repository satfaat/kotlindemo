package syntax.conditionsLoops

class WhenStatement {
}

private const val count = 40
private val quantity = 3

fun main() {
    val answerString = when {
        count == 42 -> "I have the answer."
        count > 35 -> "The answer is close."
        else -> "The answer eludes me."
    }

    println(answerString)

    val pricePerBook = when (quantity) {
        1 -> 19.99
        2 -> 18.99
        3, 4 -> 16.99
        else -> 14.99
    }

}
