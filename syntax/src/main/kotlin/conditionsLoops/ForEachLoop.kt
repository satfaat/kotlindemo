package syntax.conditionsLoops

//While loops run while a given condition is true.
//For loops run over a range of values or items.

fun main() {
    (1 until 5).forEach {
        println(it)
    }
    println()
    for (x in 10 downTo 1) print(x)
    println()
    for (x in 1..10 step 2) print(x)
}