package syntax.conditionsLoops

fun main() {

    val animals = listOf("fox")
    var animal: String = ""

    if (animals.size > 1) {
        println("Retrieving payment sums for multiple payments not supported.")
    } else {
        animal = animals[0]
    }
    println("Good by, I got $animal")

//    val animal = if (animals.size > 1) {
//        println("Retrieving payment sums for multiple payments not supported.")
//        "" // Return an empty string if multiple animals
//    } else {
//        animals[0] // Access the first element if single animal
//    }
//    println("Good by, I got $animal")
}