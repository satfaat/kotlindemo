package syntax.conditionsLoops

// Less than: <
// Greater than: >
// Less than or equal to: <=
// Greater than or equal to: >=
// Not equal to: !=

// Nullable types are variables that can hold null.
// Non-null types are variables that can't hold null.


val trafficLightColor = "Yellow"
val x = 3
var message: String? = null

enum class TrafficLightColor {
    RED,
    YELLOW,
    GREEN
}

fun main() {

    var message1 = if (trafficLightColor == "Red") {
        println("Stop")
    } else if (trafficLightColor == "Yellow") {
        println("Slow")
    } else if (trafficLightColor == "Green") {
        println("Go")
    } else {
        println("Invalid traffic-light color")
    }

    // when you deal with multiple branches, you can use the when statement
    // instead of the if/else statement because it improves readability
    // when statements are preferred when there are more than two branches to consider
    when (trafficLightColor) {
        "Red" -> println("Stop")
        "Yellow", "Amber" -> println("Slow")
        "Green" -> println("Go")
        else -> println("Invalid traffic-light color")
    }

    when (TrafficLightColor.GREEN) {
        TrafficLightColor.RED -> println("Stop")
        TrafficLightColor.YELLOW -> println("Slow")
        TrafficLightColor.GREEN -> println("Go")
        else -> println("Invalid traffic-light color")
    }

    when (x) {
        2, 3, 5, 7 -> println("x is a prime number between 1 and 10.")
        in 1..10 -> println("x is a number between 1 and 10, but not a prime number.")
        is Int -> println("x is an integer number, but not between 1 and 10.")
        else -> println("x isn't a prime number between 1 and 10.")
    }

    val message =
        if (trafficLightColor == "Red") "Stop"
        else if (trafficLightColor == "Yellow") "Slow"
        else if (trafficLightColor == "Green") "Go"
        else "Invalid traffic-light color"

    println(message)

    val message3 = when (trafficLightColor) {
        "Red" -> "Stop"
        "Yellow", "Amber" -> "Slow"
        "Green" -> "Go"
        else -> "Invalid traffic-light color"
    }
}