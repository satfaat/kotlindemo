package syntax.conditionsLoops


fun f1(int: Int): Int {
    var i = int
    return ++i + i++
}

fun f2(int: Int): Int {
    var i = int
    return ++i - i++
}

fun f3(int: Int): Int {
    var i = int
    return ++i + i++ - i--
}

fun f4(int: Int): Int {
    var i = int
    return --i + i--
}

fun main() {
    var i = 10

//    val preInc = ++i // is 11
//    val inc = i++ + ++i //22
//    println("pre increment: $inc")
//    println("post decrement: $postDec")
//    val postInc = i++
//    val postDec = i-- // 10
//    // --i is 9

//    println("post increment: $postInc")

    println(++i)

    val step1 = ++i + i++
    val st2 = step1 - 2
    val st3 = st2 * i--
    val result = ++i + i++ - 2 * i--

    println("step1: $step1")
    println("step2: $st2")
    println("step3: $st3")
    println("result: $result")
}