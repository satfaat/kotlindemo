package file

import java.io.IOException
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.logging.Logger

object FileHandler {
    private val logger: Logger = Logger.getLogger(FileHandler::class.java.name)

    /**
     * Converts a resource path to its absolute file system path.
     *
     * @param resourcePath The path to the resource within the classpath.
     * @return The absolute file system path to the resource.
     * @throws IllegalArgumentException If the resource is not found in the classpath.
     * @throws java.net.URISyntaxException If the resource URL cannot be converted to a URI.
     */
    fun resourcePathToAbsolutePath(resourcePath: String): Path {
        val url: URL = ClassLoader.getSystemResource(resourcePath)
            ?: throw IllegalArgumentException("Resource not found: $resourcePath") // Throw if null
        val path: Path = try {
            Paths.get(url.toURI())
        } catch (e: java.net.URISyntaxException) {
            throw java.net.URISyntaxException(url.toString(), "Invalid URI syntax")
        }
        return path.toAbsolutePath()
    }


    /**
     * Reads the entire content of a file into a String.
     *
     * This function attempts to read all bytes from the specified file path and convert
     * them into a String. If the file cannot be read (e.g., it doesn't exist, or there
     * are permission issues), an error is logged, and an empty string is returned.
     *
     * @param filePath The path to the file to be read.
     * @return The content of the file as a String, or an empty string if an error occurred.
     * @throws SecurityException If a security manager exists and its `checkRead` method
     * denies read access to the file.
     */
    fun getFileContent(filePath: Path): String {
        return try {
            String(Files.readAllBytes(filePath))
        } catch (e: IOException) {
            logger.severe("Error reading file: $filePath")
            ""
        }
    }
}