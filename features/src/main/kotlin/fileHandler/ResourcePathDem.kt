package fileHandler

import java.io.File
import java.io.IOException
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


private val resourcePath = "android-caps.json"

private val url: URL = ClassLoader.getSystemResource(resourcePath)
    ?: throw IllegalArgumentException("Resource not found: $resourcePath") // Throw if null
private val path: Path = Paths.get(url.toURI())

// Part 2
object F {
    val loader: ClassLoader = javaClass.classLoader
    val fileUrl: URL = loader.getResource(resourcePath)
        ?: throw IOException("File not found: $resourcePath")
    val file: File = File(fileUrl.file)
    val fileToString: String = String(Files.readAllBytes(file.toPath()))
}


fun main() {
    println(url) // file:/D:/fs/dev.hm/kotlindemo/features/build/resources/main/android-caps.json
    println("Path: $path") // D:\fs\dev.hm\kotlindemo\features\build\resources\main\android-caps.json
    println("To absolute path ${path.toAbsolutePath()}") // D:\fs\dev.hm\kotlindemo\features\build\resources\main\android-caps.json

    println("from loader: ${F.fileUrl}") // file:/D:/
    println("File: ${F.file}") //D:\
    println("File to path: ${F.file.toPath()}")
    println("file as string: ${F.fileToString}")
}