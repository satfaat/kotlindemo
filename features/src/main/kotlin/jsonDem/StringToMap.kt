package jsonDem

import file.FileHandler
import kotlinx.serialization.json.Json
import java.io.File


val caps = """{
  "platformName": "android",
  "automationName": "uiautomator2",
  "platformVersion": "16",
  "app": "android-caps.json"
}"""

val capsMap = Json.decodeFromString<MutableMap<String, String>>(caps)
private val path: String = capsMap["app"] ?: "android-caps.json"
private val appPath: File = File(path)


private val path2 = capsMap["app"]?.let {
    FileHandler.resourcePathToAbsolutePath(it)
}

private fun updateAbsolutePath(map: MutableMap<String, String>) {
    val path = map["app"] as String
    val appPath = File(path)
    map["app"] = appPath.absolutePath
}


fun main() {
    capsMap["app"] = appPath.absolutePath

    println(capsMap)
    println(path)
    println(appPath)
    println(capsMap["app"])
    println("***")
    println(path2)
    capsMap["app"] = path2.toString()
    println(capsMap["app"])

}