
* Run `./gradlew run` to build and run the application.
* Run `./gradlew build` to only build the application.
* Run `./gradlew check` to run all checks, including tests.
* Run `./gradlew clean` to clean all build outputs.


## Src:

- https://github.com/topics/playwright?l=kotlin
- https://opensource.expediagroup.com/graphql-kotlin/docs/


## Switch to debug mode

```PowerShell
# Source directories in the list are separated by : on macos and linux and by ; on win.  
$env:PLAYWRIGHT_JAVA_SRC="<java source dirs>"  
$env:PLAYWRIGHT_JAVA_SRC="src/test/kotlin"  

$env:PWDEBUG=1  
#mvn test
```

## Gradle

```powerShell
# run test
./gradlew test

# run all package 
./gradlew test --tests testNgDem.*

# see guide
gradle help --task :app:test
gradle test -h

gradle :app:test --status

gradlew test --tests dev.faat.apex_so.test

gradle :module1:test --test "dev.faat.apex_so.test"

gradlew :app:test --tests "dev.faat.apex_so.ApexSoTest.testSwitchFromHeadToSOso3"
```


## Run single test 

```powerShell
gradle testDebug --tests="*.testSwitchFromHeadToSOso3"

gradlew :app:testDebugUnitTest --tests "dev.faat.apex_so.ApexSoTest.testBookingNumber_so54"
```

## Playwright

### Codegen

```powerShell
./gradlew playwright --args="help"

./gradlew playwright --args="codegen --help"

./gradlew playwright --args="codegen https://example.com"

./gradlew playwright --args="codegen --browser chromium --device 'iPhone 13' https://blog.codeleak.pl/"
```

###Debug

```powerShell
./gradlew test -i  --tests Test1
./gradlew test -i  --tests PwDemoTest.pomDemo

./gradlew test -i  --tests ReportTest.testDownloadTerminatedDealsReport_so32

./gradlew test -i  --tests ApexSoTest.testEditContractAndPolicy_so40
./gradlew test -i  --tests ApexSoTest.testCalcSumAccordingInsuranceAmount_so47
./gradlew test -i  --tests ApexSoTest.testForingPremiumCurrencyButPayingCurrUZS_so41

./gradlew test -i --tests TerminatePayTest.testP0302_orgTerminated_so24

./gradlew test -i --tests TerminateTest.testApprovePolicyTerminate_0301_1_so52
./gradlew test -i --tests TerminateTest.testTerminateWithRefund_0301_1_so53

./gradlew test -i  --tests OsgopOsgorTest.testOsgop365_8_third_so4
./gradlew test -i  --tests OsgopOsgorTest.testOsgor365d_roundup_so5
./gradlew test -i  --tests OsgopOsgorTest.testOsgor365d_roundFloor_so44

./gradlew test -i  --tests AgentCommissionTest.testAgentCommissionFor_1440_so51
./gradlew test -i  --tests AgentCommissionTest.testAgentCommissionForOsgovts_with_limit_so49

./gradlew test -i  --tests OsgovtsNewPersonTest.testNewOwnerNotResident_so13

./gradlew test -i  --tests BindUnbindBankPayTest.testBindUnbindOneInvoice_so36
./gradlew test -i  --tests BindUnbindBankPayTest.testBindUnbindFiveInvoices_so37

$env:PWDEBUG=1 $env:PLAYWRIGHT_JAVA_SRC=./src/test/kotlin ./gradlew test --tests LoginTest::testLoginWithBranchSO2
```


```powerShell
./gradlew test -i -Dkotest.filter.tests='KotestDem.POM demonstration'
./gradlew test -i -Dkotest.filter.specs='KotestDem'
./gradlew test -i -Dkotest.filter.tests='POM*'
./gradlew test -i --tests 'apexSo2.test.LoginTest.SO2: Login with branch'
```

## Allure report

```powerShell
allure serve .\build\allure-results
```