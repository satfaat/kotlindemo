plugins {
    kotlin("jvm")
    kotlin("plugin.serialization") version "2.1.0"
    id("io.qameta.allure") version "2.11.2"
//    id("groovy")
}

group = "dev.dem"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib:2.0.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // database
    implementation(libs.exposed.core)
    implementation("org.xerial:sqlite-jdbc:3.46.0.0")
    implementation(libs.exposed.dao)
    implementation(libs.exposed.kotlin.datetime)
    implementation(libs.exposed.json)
    implementation(libs.exposed.jdbc)

    // json
    implementation("com.google.code.gson:gson:2.10")
    implementation("org.json:json:20240303")
    // JSON serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.1")

    // log
    implementation(libs.slf4j.api)
    implementation("ch.qos.logback:logback-classic:1.5.6")
    implementation("ch.qos.logback:logback-core:1.5.6")
    implementation(libs.kotlin.logging)

    // test playwright
    implementation(libs.playwright)

    // kotest and junit5
    testImplementation(libs.kotest)

    //testNG
    testImplementation(libs.testng)


//    //junit5
//    testRuntimeOnly(libs.junit.jupiter.engine)
//    testImplementation(libs.junit.jupiter.api)
//
//    implementation("org.apache.groovy:groovy:4.0.21")
//    // geb
//    testImplementation(libs.geb)
//    testImplementation(libs.selenium.firefox.driver)
//    testImplementation(libs.selenium.support)

    // report
    // kotest allure
    testImplementation(libs.kotest.allure)
}

tasks.test {
    // for junit
    //useJUnitPlatform()

    useTestNG()
//    filter {
//        //include all tests from package
//        includeTestsMatching("pwDemo.*")
//    }
}
kotlin {
    jvmToolchain(17)
}

// run ./gradlew helloWorld
tasks.register("helloWorld") {
    doLast {
        println("Hello world!")
    }
}

// Usage: ./gradlew playwright --args="help"
tasks.register<JavaExec>("playwright") {
    classpath(sourceSets["test"].runtimeClasspath)
    mainClass.set("com.microsoft.playwright.CLI")
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
    maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).coerceAtLeast(1)
}