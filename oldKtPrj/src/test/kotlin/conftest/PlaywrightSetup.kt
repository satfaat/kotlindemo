package conftest

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Playwright

class PlaywrightSetup {

    /**
     * Shared between all tests in the class.
     * */
    fun launchPlaywrightBrowser(): Pair<Playwright, Browser> {

        val playwright = Playwright.create()
        val browser = playwright.chromium()
            .launch(BrowserType.LaunchOptions().apply {
                headless = false
            })
        return Pair(playwright, browser)
    }
}