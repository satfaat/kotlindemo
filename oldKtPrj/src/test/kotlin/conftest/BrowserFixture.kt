package conftest

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import com.microsoft.playwright.options.BrowserChannel
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance


// Subclasses will inherit PER_CLASS behavior.
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class BrowserFixture {

    private lateinit var playwright: Playwright
    private lateinit var browser: Browser
    lateinit var page: Page

    @BeforeAll
    fun setUp() {
        playwright = Playwright.create()
        browser = playwright.chromium()
            .launch(
                BrowserType.LaunchOptions()
                    .setHeadless(false)
                    .setChannel(BrowserChannel.CHROME)
            )
        page = browser.newPage()
    }

    @AfterAll
    fun tearDown() {
        browser.close()
        playwright.close()
    }
}
