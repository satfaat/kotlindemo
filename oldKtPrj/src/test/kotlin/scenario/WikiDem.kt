package scenario

import com.microsoft.playwright.Page
import org.junit.jupiter.api.Assertions.assertEquals

class WikiDem(private val page: Page) {

    fun shouldSearchInWiki() {
        page.navigate("https://www.wikipedia.org/")
        page.locator("input[name=\"search\"]").click()
        page.locator("input[name=\"search\"]").fill("playwright")
        page.locator("input[name=\"search\"]").press("Enter")
        assertEquals("https://en.wikipedia.org/wiki/Playwright", page.url())
    }
}