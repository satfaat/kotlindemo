package kotestDem

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.startWith
import kotlin.time.Duration.Companion.seconds


class StringSpecDem : StringSpec({

    "length should return size of string".config(enabled = true, invocations = 3) {
        "hello".length shouldBe 5
        "".length shouldBe 0
    }

    "startsWith should test for a prefix" {
        "world" should startWith("wor")
    }

    "maximum of two numbers" {
        forAll(
            row(1, 5, 5),
            row(1, 0, 1),
            row(0, 0, 0)
        ) { a, b, max ->
            Math.max(a, b) shouldBe max
        }
    }

    "should use config".config(timeout = 2.seconds, invocations = 10, threads = 2) {
        "hello".length shouldBe 5
    }
})