package kotestDem

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldHaveLength


class AdditionalTest : FunSpec({

    beforeEach {
        println("Hello from $it")
    }

    afterEach {
        println("Goodbye from $it")
    }

    test("Additional test")
    {
        1 + 2 shouldBe 3
    }

    listOf(
        "sam",
        "pam",
        "tim",
    ).forEach {
        test("$it should be a three letter name") {
            it.shouldHaveLength(3)
        }
    }
})