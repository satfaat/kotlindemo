package pwDemo

import com.microsoft.playwright.BrowserContext
import com.microsoft.playwright.Page
import conftest.PlaywrightSetup
import io.kotest.common.ExperimentalKotest
import io.kotest.core.spec.style.StringSpec
import scenario.WikiDem
import tst.pages.SearchPage2


//object KotestProjectConfig : AbstractProjectConfig() {
//    override val parallelism = 3
//}

class KotestDem : StringSpec() {

    //    override fun threads(): Int = 4
//    override fun testCaseOrder(): TestCaseOrder = TestCaseOrder.Random
    @ExperimentalKotest
    override fun concurrency(): Int = 2

    init {

        var (playwright, browser) =
            PlaywrightSetup().launchPlaywrightBrowser()

        // New instance for each test method.
        lateinit var context: BrowserContext
        lateinit var page: Page

        afterSpec {
            playwright.close()
        }
        beforeEach {
            context = browser.newContext();
            page = context.newPage();
        }
        afterEach {
            context.close()
        }

        "Should search query in Wiki" {
            WikiDem(page).shouldSearchInWiki()
        }

        "POM demonstration" {
            val searchPage = SearchPage2(page)
            with(searchPage) {
                navigate()
                search("search query")
            }
        }
    }
}