package pwDemo

import conftest.BrowserFixture
import org.junit.jupiter.api.Test
import scenario.WikiDem
import tst.pages.SearchPage2


open class PWparallel : BrowserFixture() {

    @Test
    fun shouldSearchWikiTest() {
        WikiDem(page).shouldSearchInWiki()
    }

    @Test
    fun pomDemo() {
        val searchPage = SearchPage2(page)
        searchPage.navigate()
        searchPage.search("search query")
    }

}
