package pwDemo

import com.microsoft.playwright.*
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import tst.pages.SearchPage2


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SearchTest {

    private lateinit var playwright: Playwright
    private lateinit var browser: Browser
    private lateinit var context: BrowserContext
    private lateinit var page: Page

    @BeforeAll
    fun setUp() {
        playwright = Playwright.create()
        browser = playwright.chromium()
            .launch(
                BrowserType.LaunchOptions()
                    .setHeadless(false)
//                    .setChannel(BrowserChannel.CHROME)
            )
        context = browser.newContext()
        page = context.newPage()
//        page = browser.newPage()
    }

    @AfterAll
    fun tearDown(): Unit {
        browser.close()
        playwright.close()
    }


    @Test
    fun testSearchInBing() {
        val searchPage = SearchPage2(page)
        searchPage.navigate()
        searchPage.search("search query")
    }
}