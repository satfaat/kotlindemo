package pwDemo

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import scenario.WikiDem
import tst.pages.SearchPage2


// Subclasses will inherit PER_CLASS behavior.
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class PwDemoTest {

    private lateinit var playwright: Playwright
    private lateinit var browser: Browser
    private lateinit var page: Page

    @BeforeAll
    fun setUp() {
        playwright = Playwright.create()
        browser = playwright.chromium()
            .launch(
                BrowserType.LaunchOptions()
                    .setHeadless(false)
                    .setChromiumSandbox(true)
            )
        page = browser.newPage()
    }

    @AfterAll
    fun tearDown() {
        browser.close()
        playwright.close()
    }

    @Test
    fun shouldSearchWikiTest() {
        WikiDem(page).shouldSearchInWiki()
    }

    @Test
    fun pomDemo() {
        val searchPage = SearchPage2(page)
        searchPage.navigate()
        searchPage.search("search query")
    }
}