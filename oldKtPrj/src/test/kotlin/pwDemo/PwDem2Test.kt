package pwDemo

import com.microsoft.playwright.Browser
import com.microsoft.playwright.Page
import com.microsoft.playwright.junit.Options
import com.microsoft.playwright.junit.OptionsFactory
import com.microsoft.playwright.junit.UsePlaywright
import org.junit.jupiter.api.Test
import scenario.WikiDem
import tst.pages.SearchPage2
import java.nio.file.Paths


@UsePlaywright(PwDem2Test.CustomOptions::class)
class PwDem2Test {
    class CustomOptions : OptionsFactory {
        override fun getOptions(): Options {
            return Options()
                .setHeadless(false)
                .setContextOptions(
                    Browser.NewContextOptions()
                        .setRecordVideoDir(Paths.get("./build/videos/"))
                )
        }
    }

    @Test
    fun shouldSearchWikiTest(page: Page) {
        WikiDem(page).shouldSearchInWiki()
    }

    @Test
    fun pomDemo(page: Page) {
        val searchPage = SearchPage2(page)
        searchPage.navigate()
        searchPage.search("search query")
    }
}