package pwDemo

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat
import com.microsoft.playwright.options.BrowserChannel
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import tst.pages.PlaywrightDevPage
import tst.pages.TodoPage
import java.util.regex.Pattern

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TodoAppE2ETest {

    private lateinit var playwright: Playwright
    private lateinit var browser: Browser
    private lateinit var page: Page
    private lateinit var todoPage: TodoPage

    @BeforeAll
    fun setUp() {
        playwright = Playwright.create()
        browser = playwright.chromium()
            .launch(
                BrowserType.LaunchOptions()
                    .setHeadless(false)
                    .setChannel(BrowserChannel.CHROME)
            )
        page = browser.newPage()
        todoPage = TodoPage(page)
    }

    @AfterAll
    fun tearDown(): Unit {
        browser.close()
        playwright.close()
    }

    @Test
    fun testPlaywrightDevPage() {
        page = browser.newPage()
        val playwrightDevPage = PlaywrightDevPage(page)

        playwrightDevPage.navigate()
        assertThat(page).hasTitle(Pattern.compile("Playwright"))

        playwrightDevPage.clickGetStarted()
        assertThat(page).hasURL("https://playwright.dev/docs/intro")

        playwrightDevPage.verifyInstallationHeadingVisible()
    }

//    @Test
//    fun testAddTodoItem() {
//        todoPage.navigateToApp("https://example.com/todo-app")
//        todoPage.addTodoItem("Buy groceries")
//        assert(todoPage.isTodoItemDisplayed("Buy groceries"))
//
//        val screenshotPath = "screenshot.png"
//        page.screenshot(Page.ScreenshotOptions().setPath(Paths.get(screenshotPath)))
//    }
}