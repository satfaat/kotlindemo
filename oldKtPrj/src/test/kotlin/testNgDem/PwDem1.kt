package testNgDem

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import scenario.WikiDem
import tst.pages.SearchPage2

@Test(groups = ["login"])
class PwDem1 {

    private lateinit var playwright: Playwright
    private lateinit var browser: Browser
    private lateinit var page: Page

    @BeforeClass
    fun setUp() {
        playwright = Playwright.create()
        browser = playwright.chromium()
            .launch(
                BrowserType.LaunchOptions()
                    .setHeadless(false)
                    .setChromiumSandbox(true)
            )
        page = browser.newPage()
    }

    @AfterClass
    fun tearDown() {
        browser.close()
        playwright.close()
    }

    @Test(dependsOnGroups = ["login"])
    fun shouldSearchWikiTest() {
        WikiDem(page).shouldSearchInWiki()
    }

    @Test(dependsOnMethods = ["shouldSearchWikiTest"], groups = ["fast"])
    fun pomDemo() {
        val searchPage = SearchPage2(page)
        searchPage.navigate()
        searchPage.search("search query")
    }
}