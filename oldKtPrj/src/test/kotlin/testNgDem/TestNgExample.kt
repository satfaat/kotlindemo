package testNgDem

import org.testng.Assert
import org.testng.annotations.Test


class TestNgExample {

    @Test(groups = ["login"])
    fun test1() {
        val sum = 5
        Assert.assertEquals(sum, 5)
    }
}