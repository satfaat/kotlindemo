package tst.pages

import com.microsoft.playwright.Page

class PlaywrightDevPage(private val page: Page) {

    fun navigate() {
        page.navigate("http://playwright.dev")
    }

    fun clickGetStarted() {
        page.click("text=Get Started")
    }

    fun verifyInstallationHeadingVisible() {
        page.isVisible("text=Installation")
    }
}