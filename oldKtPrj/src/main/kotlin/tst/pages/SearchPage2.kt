package tst.pages

import com.microsoft.playwright.Locator
import com.microsoft.playwright.Page

class SearchPage2() {
    private var page: Page? = null
    private var searchTermInput: Locator? = null

    constructor(page: Page) : this() {
        this.page = page
        this.searchTermInput = page.locator("[aria-label='Enter your search term']")
    }

    fun navigate() {
        page?.navigate("https://bing.com")

    }

    fun search(text: String?) {
        searchTermInput?.fill(text)
        searchTermInput?.press("Enter")
    }
}