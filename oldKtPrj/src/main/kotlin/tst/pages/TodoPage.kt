package tst.pages

import com.microsoft.playwright.Page

class TodoPage(private val page: Page) {

    private val inputFieldSelector = "input.new-todo"
    private val todoItemSelector = "li.todo-item"

    fun navigateToApp(url: String) {
        page.navigate(url)
        page.waitForLoadState()
    }

    fun addTodoItem(todoText: String) {
        val inputField = page.querySelector(inputFieldSelector)
        inputField.type(todoText)
        inputField.press("Enter")
    }

    fun isTodoItemDisplayed(todoText: String): Boolean {
        return page.waitForSelector(todoItemSelector).textContent() == todoText
    }
}