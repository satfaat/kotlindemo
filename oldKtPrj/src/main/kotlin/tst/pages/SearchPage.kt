package tst.pages

import com.microsoft.playwright.Page


class SearchPage(private val page: Page) {

    // Locators
    val searchTermInput = page.locator("[aria-label='Enter your search term']")

    // Method that will open page with url
    fun navigate() {
        page.navigate("https://bing.com")

    }

    // method that will add text and press button
    fun search(text: String) {
        searchTermInput.fill(text)
        searchTermInput.press("Enter")
    }
}