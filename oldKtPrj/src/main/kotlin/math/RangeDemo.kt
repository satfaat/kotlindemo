package math

class RangeDemo {
}

private val numberRange: IntRange = 1..10  // Includes 1 and 10
private val charRange: CharRange = 'a'..'z'  // Includes 'a' and 'z'
private val numberRange2 = 1 until 10  // Includes 1 to 9 (excluding 10)
private val evenNumbers = 2..10 step 2  // Includes even numbers from 2 to 10
private val range = 1.rangeTo(4)

fun main() {

    println(numberRange)
    println(charRange)
    println(evenNumbers)
}