package math

private fun add(a: Int, b: Int): Int {
    return a + b
}

fun main() {
    println(::add.invoke(5, 10))
}