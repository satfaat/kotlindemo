package math

import kotlin.random.Random.Default.nextInt

val r1 = nextInt()
val r2 = Math.random()


fun main() {
    println(r1 * 2)
    println(r2 * 2)
}
