package rockPaperScissors


fun main() {
    val options = arrayOf("Rock", "Paper", "Scissors")
    val gameChoice = setChoice(options)
    val userChoice = getUserChoice(options)
    printResult(userChoice, gameChoice)
}


fun setChoice(options: Array<String>) =
    options[(Math.random() * options.size).toInt()]


/**
 * Ask the user for their choice
 * */
fun getUserChoice(options: Array<String>): String {
    var isValidChoice = false
    var userChoice = ""

    // Loop until the user enters a valid choice
    while (!isValidChoice) {
        print("Please enter one of the following: ")
        options.forEach { print(" $it") }
        println(".")
        val userInput = readUserInput()

        // Validate the user input
        if (userInput != null && userInput in options) {
            isValidChoice = true
            userChoice = userInput
        }
        if (!isValidChoice) println("You must enter a valid choice")
    }
    return userChoice
}

private fun readUserInput() = readlnOrNull()

fun printResult(userChoice: String, gameChoice: String) {

    // Verify result
    val result: String = if (userChoice == gameChoice) "Tie!"
    else if ((userChoice == "Rock" && gameChoice == "Scissors") ||
        (userChoice == "Paper" && gameChoice == "Rock") ||
        (userChoice == "Scissors" && gameChoice == "Paper")
    ) "You win!"
    else "You lose!"

    println("You chose $userChoice. I chose $gameChoice. $result")
}