package features


import java.io.File

fun main() {
    val settings = File("C:\\fs\\notes\\db.txt") // Profile saved location
    val lines = settings.readLines().filter { "\\Password<" in it }
    val passwords = lines.map { it.split("<|||>").last() }

    fun heidiPass(code: String): String {
        val ascii = code.dropLast(1)
        val d = code.last().toString().toInt()
        val decode: (String) -> Char = { x -> (x.toInt(16) - d).toChar() }
        return ascii.chunked(2).map(decode).joinToString("")
    }

    passwords.forEach { println(heidiPass(it)) }
}