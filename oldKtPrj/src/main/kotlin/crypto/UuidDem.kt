package crypto

import java.util.UUID


val uuid: UUID = UUID.randomUUID()
val uuidToStr = uuid.toString().replace("-", "")
val uuidShort = uuid.toString().take(8)

fun main() {

    println("UUID: $uuid")
    println("UUID2: $uuidToStr")
    println("UUID short: $uuidShort")

}